package practica6.brayanpincayv.facci.sensores;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity {

    Button botonSensores;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);


        botonSensores=(Button)findViewById(R.id.btnSensores);

        botonSensores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cambiar =new Intent(getApplicationContext(), ActividadSensorAcelerometro.class);
                startActivity(cambiar);
            }
        });
    }
}
